object Form1: TForm1
  Left = 192
  Top = 117
  Width = 928
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_nome: TLabel
    Left = 64
    Top = 24
    Width = 98
    Height = 20
    Caption = 'Insira o Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 224
    Top = 24
    Width = 30
    Height = 20
    Caption = 'Tipo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 384
    Top = 24
    Width = 25
    Height = 20
    Caption = 'Cor'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object txf_nome: TEdit
    Left = 64
    Top = 48
    Width = 153
    Height = 25
    TabOrder = 0
  end
  object txf_tipo: TEdit
    Left = 224
    Top = 48
    Width = 153
    Height = 25
    TabOrder = 1
  end
  object txf_cor: TEdit
    Left = 384
    Top = 48
    Width = 177
    Height = 25
    TabOrder = 2
  end
  object lst_lista: TListBox
    Left = 64
    Top = 120
    Width = 497
    Height = 297
    ItemHeight = 13
    TabOrder = 3
  end
  object btn_inserir: TButton
    Left = 64
    Top = 80
    Width = 89
    Height = 33
    Caption = 'Inserir'
    TabOrder = 4
    OnClick = btn_inserirClick
  end
  object btn_excluir: TButton
    Left = 160
    Top = 80
    Width = 89
    Height = 33
    Caption = 'Excluir'
    TabOrder = 5
    OnClick = btn_excluirClick
  end
  object btn_atualizar: TButton
    Left = 256
    Top = 80
    Width = 97
    Height = 33
    Caption = 'Atualizar'
    TabOrder = 6
    OnClick = btn_atualizarClick
  end
  object Button1: TButton
    Left = 360
    Top = 80
    Width = 105
    Height = 33
    Caption = 'Salvar'
    TabOrder = 7
    OnClick = Button1Click
  end
end
